import React, { Component } from 'react';
import '../css/Section5.css';
import logo1 from '../assets/img/section5-logo1.png';
import logo2 from '../assets/img/section5-logo2.png';
import logo3 from '../assets/img/section5-logo3.png';
import logo4 from '../assets/img/section5-logo4.png';
import logo5 from '../assets/img/section5-logo5.png';

class Section5 extends Component {
	render() {
		return (
			<section className="Section5">
				<div className="container-fluid">
					<div className="row">
						<div className="col-xs-12">
			
							<div className="container">
								<div className="row">
									<div className="col-xs-12 col-sm-4 text-center">
										<div className="pic"></div>
										<h3>Johnatan Doe</h3>
										<h4>Co-founder</h4>
										<p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pellentesque imperdiet tortor."</em></p>
									</div>
									<div className="col-xs-12 col-sm-4 text-center">
										<div className="pic"></div>
										<h3>Johnatan Doe</h3>
										<h4>Co-founder</h4>
										<p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pellentesque imperdiet tortor."</em></p>
									</div>
									<div className="col-xs-12 col-sm-4 text-center">
										<div className="pic"></div>
										<h3>Johnatan Doe</h3>
										<h4>Co-founder</h4>
										<p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pellentesque imperdiet tortor."</em></p>
									</div>
								</div>

								<div className="row">
									<div className="col-xs-12 heightDivider"></div>
								</div>

								<div className="row">
									<div className="col-xs-6 col-sm-4 col-md-2 col-md-offset-1 text-center">
										<img src={logo1} className="logo img-responsive" alt="Logo 1" />
									</div>
									<div className="col-xs-6 col-sm-4 col-md-2 text-center">
										<img src={logo2} className="logo img-responsive" alt="Logo 2" />
									</div>
									<div className="col-xs-6 col-sm-4 col-md-2 text-center">
										<img src={logo3} className="logo img-responsive" alt="Logo 3" />
									</div>
									<div className="col-xs-6 col-sm-4 col-sm-offset-2 col-md-2 col-md-offset-0 text-center">
										<img src={logo4} className="logo img-responsive" alt="Logo 4" />
									</div>
									<div className="col-xs-6 col-xs-offset-3 col-sm-4 col-sm-offset-0 col-md-2 col-md-offset-0 text-center">
										<img src={logo5} className="logo img-responsive" alt="Logo 5" />
									</div>
								</div>
							</div>
			
						</div>
					</div>
				</div>
			</section>
			);
	}
}

export default Section5;
