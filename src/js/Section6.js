import React, { Component } from 'react';
import '../css/Section6.css';
import browser from '../assets/img/section6-browser.png';

class Section6 extends Component {
	render() {
		return (
			<section className="Section6">
				<div className="container-fluid">
					<div className="row">
						<div className="col-xs-12">
			
							<div className="container">
								<div className="row">
									<div className="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
										<p className="subTitle">New features</p>
										<h2>Some awesome features</h2>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pellentesque imperdiet tortor. Cras bibendum ipsum sed hendrerit eleifend. Ut ipsum eros, varius et iaculis in, vehicula rhoncus ipsum.</p>
									</div>
								</div>

								<div className="row">
									<div className="col-xs-12 heightDivider"></div>
								</div>

								<div className="row">
									<div className="col-xs-12 col-sm-8 col-sm-offset-2 text-center">
										<img src={browser} className="img-responsive" alt="New features" />
									</div>
								</div>
							</div>
			
						</div>
					</div>
				</div>
			</section>
			);
	}
}

export default Section6;
