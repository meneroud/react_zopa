import React, { Component } from 'react';
import '../css/Section8.css';
import logo from '../assets/img/section7-logo.png';
import btnIcon from '../assets/img/section7-buttonIcon.png';
import some1 from '../assets/img/section7-logo1.png';
import some2 from '../assets/img/section7-logo2.png';
import some3 from '../assets/img/section7-logo3.png';
import some4 from '../assets/img/section7-logo4.png';

class Section8 extends Component {
	render() {
		return (
			<section className="Section8">
				<div className="container-fluid">
					<div className="row">
						<div className="col-xs-12">
			
							<div className="container">
								<div className="row">
									<div className="col-xs-12 col-sm-3 text-left">
										<img src={logo} className="img-responsive logo" alt="Company name" />
										<p>Lorem ipsum dolor sit amet<br />Lorem ipsum dolor sit amet<br />USA & CAN: 1-888-123-4567<br />Address: 34 Brokel Rd. NY<br /><br /></p>
									</div>
									<div className="col-xs-6 col-sm-2 text-left">
										<h2>Support</h2>
										<a href="#">Help Center</a>
										<a href="#">Get Started</a>
										<a href="#">Contact Us</a>
										<br /><br />
									</div>
									<div className="col-xs-6 col-sm-2 text-left">
										<h2>About us</h2>
										<a href="#">About us</a>
										<a href="#">Terms of use</a>
										<a href="#">Privacy Policy</a>
										<br /><br />
									</div>
									<div className="col-xs-12 col-sm-5 col-md-4 text-left">
										<h2>Get Newsletter</h2>
										<form className="form-inline">
											<input type="email" className="form-control" placeholder="EMAIL" />
											<button type="submit" className="btn btn-danger"><img src={btnIcon} className="img-responsive btnIcon" alt="Submit" /></button>
										</form>
										<div className="clearfix"></div>
										<div className="text-center">
											<a href="#" className="some"><img src={some1} className="img-responsive someImg" alt="Some1" /></a>
											<a href="#" className="some"><img src={some2} className="img-responsive someImg" alt="Some2" /></a>
											<a href="#" className="some"><img src={some3} className="img-responsive someImg" alt="Some3" /></a>
											<a href="#" className="some"><img src={some4} className="img-responsive someImg" alt="Some4" /></a>
										</div>
									</div>
								</div>
							</div>
			
						</div>
					</div>
				</div>
			</section>
			);
	}
}

export default Section8;
