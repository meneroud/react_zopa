import React, { Component } from 'react';
import WebFont from 'webfontloader';
import Section1 from './Section1';
import Section2 from './Section2';
import Section3 from './Section3';
import Section4 from './Section4';
import Section5 from './Section5';
import Section6 from './Section6';
import Section7 from './Section7';
import Section8 from './Section8';

WebFont.load({
  google: {
    families: ['Roboto:100,200,400, 600', 'sans-serif']
  }
});

class App extends Component {
  render() {
    return (
      <div className="App">
        <Section1></Section1>
        <Section2></Section2>
        <Section3></Section3>
        <Section4></Section4>
        <Section5></Section5>
        <Section6></Section6>
        <Section7></Section7>
        <Section8></Section8>
      </div>
    );
  }
}

export default App;
