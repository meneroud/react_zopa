import React, { Component } from 'react';
import '../css/Section2.css';

class Section2 extends Component {
	render() {
		return (
			<section className="Section2">
				<div className="container-fluid">
					<div className="row">
						<div className="col-xs-12">
			
							<div className="container">
								<div className="row">
									<div className="col-xs-12 col-md-6 text-left">
										<p className="subTitle">New design</p>
										<h2>There is no other platforms<span className="hidden-xs"><br /></span> for you as like <span className="red">....</span></h2>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pellentesque imperdiet tortor. Cras bibendum ipsum sed hendrerit eleifend. Ut ipsum eros, varius et iaculis in, vehicula rhoncus ipsum. Sed eu quam fringilla, fermentum velit et, iaculis metus. Vivamus sit amet mauris elementum, pellentesque ligula et, rhoncus ex. <br /><br /></p>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pellentesque imperdiet tortor. Cras bibendum ipsum sed hendrerit eleifend. Ut ipsum eros, varius et iaculis in, vehicula rhoncus ipsum. Sed eu quam fringilla, fermentum velit et, iaculis metus. Vivamus sit amet mauris elementum, pellentesque ligula et, rhoncus ex.</p>
									</div>
									<div className="hidden-xs col-sm-6">
										
									</div>
								</div>
							</div>
			
						</div>
					</div>
				</div>
			</section>
			);
	}
}

export default Section2;
