import React, { Component } from 'react';
import '../css/Section4.css';
import macbook from '../assets/img/section4-macbook.png';

class Section4 extends Component {
	render() {
		return (
			<section className="Section4">
				<div className="container-fluid">
					<div className="row">
						<div className="col-xs-12">
			
							<div className="container">
								<div className="row">
									<div className="col-xs-12 col-sm-6 text-center">
										<img src={macbook} className="img-responsive macbook" alt="New design" />
									</div>
									<div className="col-xs-12 col-sm-6 text-left">
										<p className="subTitle">New design</p>
										<h2>Responsive design, just<span className="hidden-xs"><br /></span> need your tap <span className="red">....</span></h2>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pellentesque imperdiet tortor. Cras bibendum ipsum sed hendrerit eleifend. Ut ipsum eros, varius et iaculis in, vehicula rhoncus ipsum.</p>
									</div>
								</div>
							</div>
			
						</div>
					</div>
				</div>
			</section>
			);
	}
}

export default Section4;
