import React, { Component } from 'react';
import '../css/Section1.css';
import playBtn from '../assets/img/header-icon-play.png';

class Section1 extends Component {
	render() {
		return (
			<section className="Section1">
				<div className="container-fluid">
					<div className="row">
						<div className="col-xs-12">
			
							<div className="container">
								<div className="row">
									<div className="col-xs-12 text-center">
										<h1>There is no other<br />platforms for you as like</h1>
										<a href="#" className="playBtnLink"><img src={playBtn} className="playBtn" alt="Play" /></a>
										<div className="clearfix"></div>
										<a href="#" className="btn btn-danger">Try now</a>
										<p className="note">* No need to add card details</p>
									</div>
								</div>
							</div>
			
						</div>
					</div>
				</div>
			</section>
			);
	}
}

export default Section1;
