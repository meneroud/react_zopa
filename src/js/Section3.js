import React, { Component } from 'react';
import '../css/Section3.css';
import icon1 from '../assets/img/section3-icon1.png';
import icon2 from '../assets/img/section3-icon2.png';
import icon3 from '../assets/img/section3-icon3.png';

class Section3 extends Component {
	render() {
		return (
			<section className="Section3">
				<div className="container-fluid">
					<div className="row">
						<div className="col-xs-12">
			
							<div className="container">
								<div className="row">
									<div className="col-xs-12 text-center">
										<p className="subTitle">New features</p>
										<h2>Some awesome features</h2>
									</div>
								</div>

								<div className="row">
									<div className="col-xs-12 col-sm-4 text-center">
										<img src={icon1} className="icon img-responsive" alt="Some awesome features" />
										<h3>Some awesome features</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pellentesque imperdiet tortor.</p>
									</div>
									<div className="col-xs-12 col-sm-4 text-center">
										<img src={icon2} className="icon img-responsive" alt="Some awesome features" />
										<h3>Some awesome features</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pellentesque imperdiet tortor.</p>
									</div>
									<div className="col-xs-12 col-sm-4 text-center">
										<img src={icon3} className="icon img-responsive" alt="Some awesome features" />
										<h3>Some awesome features</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pellentesque imperdiet tortor.</p>
									</div>
								</div>
							</div>
			
						</div>
					</div>
				</div>
			</section>
			);
	}
}

export default Section3;
