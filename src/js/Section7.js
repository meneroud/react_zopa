import React, { Component } from 'react';
import '../css/Section7.css';
import arrow from '../assets/img/arrow.jpg';

class Section7 extends Component {
	render() {
		return (
			<section className="Section7">
				<div className="container-fluid">
					<div className="row">
						<div className="col-xs-12">
			
							<div className="container">
								<div className="row">
									<div className="col-xs-12 text-center">
										<p className="subTitle">New features</p>
										<h2>Over 1000 designers are using ...</h2>
									</div>
								</div>

								<div className="row heightDivider">
									<div className="col-xs-12 col-sm-3 col-sm-offset-9 text-left">
										<p className="note">
											<img src={arrow} className="arrow" alt="Free trial" />
											30 days free trial.
										</p>
									</div>
								</div>

								<div className="row">
									<form>
										<div className="col-xs-12 col-sm-3">
											<input type="text" className="form-control" placeholder="FULL NAME" />
										</div>

										<div className="col-xs-12 col-sm-3">
											<input type="email" className="form-control" placeholder="YOUR EMAIL" />
										</div>

										<div className="col-xs-12 col-sm-3">
											<input type="password" className="form-control" placeholder="PASSWORD" />
										</div>

										<div className="col-xs-12 col-sm-3">
											<button type="submit" className="btn btn-block btn-danger">Try now</button>
										</div>
									</form>
								</div>

								<div className="row">
									<div className="col-xs-12 text-center">
										<p className="note note2">By signing up you agree to our <a href="#" className="link">terms & Services</a>.</p>
									</div>
								</div>
							</div>
			
						</div>
					</div>
				</div>
			</section>
			);
	}
}

export default Section7;
